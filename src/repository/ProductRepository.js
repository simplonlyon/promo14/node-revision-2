import { Product } from "../entity/Product";
import { connection } from "./connection";
/**
 * On peut utiliser une classe pour regrouper ensembles les méthodes
 * d'un même repository, c'est pas obligatoire, mais c'est pas beaucoup
 * de boulot non plus.
 */
export class ProductRepository {

    /**
     * Méthode pour récupérer les produits d'une catégorie donnée
     * @param {string} category la catégorie dont on souhaite les produits
     * @returns {Promise<Product[]>}
     */
    static async findByCategory(category, limit = 25, offset = 0) {
        const [rows] = await connection.query('SELECT * FROM product WHERE category=? LIMIT ? OFFSET ?', [category, limit, offset]);
        const products = [];
        // @ts-ignore
        for(let row of rows) {
            //ici on fait correspondre les différentes colonnes de la table de la bdd
            //avec les arguments attendus par le constructeur du Product.
            products.push(new Product(row['label'], row['category'], row['price'], row['description'], row['id']));
        }
        return products;

        //On peut remplacer les lignes 17->23 par ça si on veut
        //return rows.map(row => new Product(row['label'], row['category'], row['price'], row['description'], row['id']));
    }

    /**
     * Methode d'ajout d'un nouveau produit
     * @param {object} newProduct 
     */
    static async add(newProduct) {
        const [raw] = await connection.query('INSERT INTO product (label, category, price, description) VALUES (?,?,?,?)' , [newProduct.label, newProduct.category, newProduct.price, newProduct.description]);
    }

    /**
     * Méthode de mise à jour d'un produit
     * @param {object} productI 
     */
    static async update(productI) {
        const [raw] = await connection.query('UPDATE product SET label=?, category=?, price=?, description=? WHERE product.id = ?' , [productI.label, productI.category, productI.price, productI.description, productI.id]);
    }
}