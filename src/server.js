import express from 'express';
import { productController } from './controller/product-controller';

export const server = express();

server.use(express.json());


server.use('/api/product', productController);